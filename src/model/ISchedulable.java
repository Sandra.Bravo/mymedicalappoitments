package model;

import java.util.Date;

public interface ISchedulable {

    void schedule(Date date, String time);

    default void test1(){
        System.out.println("DEFAULT METHOD INTERFACE");
    }

    private String test2(){
        return "PRIVATE METHOD INTERFACE";
    }
}
