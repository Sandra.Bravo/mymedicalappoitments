public enum Days {
    SUNDAY("Domingo"),
    MONDAY("Lunes"),
    TUESDAY("Martes"),
    WEDNESDAY("Miércoles"),
    THURSDAY("Jueves"),
    FRIDAY("Viernes"),
    SATURDAY("Sábado");

    private String spanish;
    Days(String s) {
        this.spanish = s;
    }

    public String getSpanish(){
        return spanish;
    }

}
